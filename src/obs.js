const OBSWebSocket = require('obs-websocket-js');
const obs = new OBSWebSocket();

let messagebus;

obs.on('error', err => {
    console.error('Error:', err);
});

const setMessageBus = (bus) => {
    messagebus = bus;
}

const connect = (config) => obs.connect({ address: `${config.host}:${config.port}` });

const configureAudio = (config) => {
    Object.keys(config).forEach((source) => {
        messagebus.subscribe(source, (input) => {
            obs.setVolume({'source': source, volume: input / 128});
        });
    })
}

const configureVideo = (config) => {
    Object.keys(config).forEach((scene) => {
        messagebus.subscribe(scene, (input) => {
            obs.setCurrentScene({'scene-name': scene});
        });
    })
}

module.exports = {
    setMessageBus,
    connect,
    configureAudio,
    configureVideo
}