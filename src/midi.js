const midi = require('midi');

let messagebus;

const input = new midi.input();

const setMessageBus = (bus) => {
    messagebus = bus;
};

const configureAudio = sources => {
    input.on('message', (deltaTime, message) => {
        Object.keys(sources).forEach(input => {
            if(message[1] == sources[input]) {
                messagebus.emit(input, message[2])
            }
        })
    })
}

const configureVideo = scenes => {
    input.on('message', (deltaTime, message) => {
        Object.keys(scenes).forEach(input => {
            if(message[1] == scenes[input]) {
                messagebus.emit(input)
            }
        })
    })
}

const startListening = (config) => {
    input.openPort(config.device)
}

module.exports = {
    setMessageBus,
    configureAudio,
    configureVideo,
    startListening
}
