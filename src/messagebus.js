const listeners = [];

const subscribe = (messageName, eventHandler) => {
    if (!listeners[messageName]) {
        listeners[messageName] = [];
    }

    listeners[messageName].push(eventHandler)
}

const emit = (messageName, messageData) => {
    if (!!listeners[messageName]) {
        console.log("Emitting", messageName, messageData);
        listeners[messageName].forEach(eventHandler => eventHandler(messageData));
    }
}

module.exports = {
    subscribe,
    emit
}