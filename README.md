**UNTESTED**

# OBS Websocket / Midi Controller

This application provides a configurable interface between OBS and a Midi Controller.

 * Configuration can be found in config.json
 * Scenes can be controlled by one set of Midi Buttons
 * Volume can be controlled by a set of Variable inputs (likely pots)
