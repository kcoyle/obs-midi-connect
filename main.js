const messagebus = require('./src/messagebus');
const midi = require('./src/midi');
const obs = require('./src/obs');

const config = require('./config.json');

console.info("Midi Controller")
midi.setMessageBus(messagebus);
midi.configureVideo(config.mapping.scenes);
midi.configureAudio(config.mapping.audio);
midi.startListening(config.midi);
console.info("Midi Controller: Check")

console.info("OBS Connection")
obs.setMessageBus(messagebus);
obs.connect(config.obs);
obs.configureVideo(config.mapping.scenes);
obs.configureAudio(config.mapping.audio);
console.info("OBS Connection: Check")

console.info("...")
console.info("ENGAGE!")