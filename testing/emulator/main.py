#!/usr/bin/env python

from PyQt5.QtCore import QDateTime, Qt, QTimer
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
        QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
        QVBoxLayout, QWidget, QMainWindow, QAction)
import time
import rtmidi

minPotRange = 0
maxPotRange = 0

buttons = {
    'line1': {
        'a': 1, 'b': 2, 'c': 3, 'd': 4,
        'e': 5, 'f': 6, 'g': 7, 'h': 8,
    },
    'line2': {
        'a': 1, 'b': 2, 'c': 3, 'd': 4,
        'e': 5, 'f': 6, 'g': 7, 'h': 8,
    },
    'line3': {
        'a': 1, 'b': 2, 'c': 3, 'd': 4,
        'e': 5, 'f': 6, 'g': 7, 'h': 8,
    },
    'line4': {
        'a': 1, 'b': 2, 'c': 3, 'd': 4,
        'e': 5, 'f': 6, 'g': 7, 'h': 8,
    }
}

class MidiWidgets(QDialog):
    def __init__(self, parent=None):
        super(MidiWidgets, self).__init__(parent)

        self.createleftGroupBox()
        self.createrightGroupBox()

        mainLayout = QHBoxLayout()
        mainLayout.addWidget(self.leftGroupBox)
        mainLayout.addWidget(self.rightGroupBox)
        self.setLayout(mainLayout)

        self.setWindowTitle("Video Switch Midi Controller")

        # Connect to Midi
        self.midiout = rtmidi.MidiOut()
        available_ports = self.midiout.get_ports()

        if available_ports:
            self.midiout.open_port(0)
        else:
            self.midiout.open_virtual_port("My virtual output")

    def createleftGroupBox(self):
        self.leftGroupBox = QGroupBox("")

        buttonA = QPushButton("A")
        buttonB = QPushButton("B")
        buttonC = QPushButton("C")
        buttonD = QPushButton("D")
        buttonE = QPushButton("E")
        buttonF = QPushButton("F")
        buttonG = QPushButton("G")
        buttonH = QPushButton("H")

        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding);
        buttonA.setSizePolicy(sizePolicy)
        buttonB.setSizePolicy(sizePolicy)
        buttonC.setSizePolicy(sizePolicy)
        buttonD.setSizePolicy(sizePolicy)
        buttonE.setSizePolicy(sizePolicy)
        buttonF.setSizePolicy(sizePolicy)
        buttonG.setSizePolicy(sizePolicy)
        buttonH.setSizePolicy(sizePolicy)

        buttonA.clicked.connect(self.flatButtonA)
        buttonB.clicked.connect(self.flatButtonB)
        buttonC.clicked.connect(self.flatButtonC)
        buttonD.clicked.connect(self.flatButtonD)
        buttonE.clicked.connect(self.flatButtonE)
        buttonF.clicked.connect(self.flatButtonF)
        buttonG.clicked.connect(self.flatButtonG)
        buttonH.clicked.connect(self.flatButtonH)

        layout = QVBoxLayout()
        
        layoutTop = QHBoxLayout()
        layoutTop.addWidget(buttonA)
        layoutTop.addWidget(buttonB)
        layoutTop.addWidget(buttonC)
        layoutTop.addWidget(buttonD)
        layout.addLayout(layoutTop)

        layoutBottom = QHBoxLayout()
        layoutBottom.addWidget(buttonE)
        layoutBottom.addWidget(buttonF)
        layoutBottom.addWidget(buttonG)
        layoutBottom.addWidget(buttonH)
        layout.addLayout(layoutBottom)

        self.leftGroupBox.setLayout(layout)

    def createrightGroupBox(self):
        self.rightGroupBox = QGroupBox("")

        dialA = QDial()
        dialB = QDial()
        dialC = QDial()
        dialD = QDial()
        dialE = QDial()
        dialF = QDial()
        dialG = QDial()
        dialH = QDial()

        dialA.valueChanged.connect(self.dialAAction)
        dialB.valueChanged.connect(self.dialBAction)
        dialC.valueChanged.connect(self.dialCAction)
        dialD.valueChanged.connect(self.dialDAction)
        dialE.valueChanged.connect(self.dialEAction)
        dialF.valueChanged.connect(self.dialFAction)
        dialG.valueChanged.connect(self.dialGAction)
        dialH.valueChanged.connect(self.dialHAction)

        layout = QVBoxLayout()
        
        layoutTop = QHBoxLayout()
        layoutTop.addWidget(dialA)
        layoutTop.addWidget(dialB)
        layoutTop.addWidget(dialC)
        layoutTop.addWidget(dialD)
        layout.addLayout(layoutTop)

        layoutBottom = QHBoxLayout()
        layoutBottom.addWidget(dialE)
        layoutBottom.addWidget(dialF)
        layoutBottom.addWidget(dialG)
        layoutBottom.addWidget(dialH)
        layout.addLayout(layoutBottom)

        self.rightGroupBox.setLayout(layout)

    def flatButtonA(self, button):
        print(self)
        print("Button A")
        note_on = [0x90, 1, 1]
        note_off = [0x80, 1, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonB(self, button):
        print(self)
        print("Button B")
        note_on = [0x90, 2, 1]
        note_off = [0x80, 2, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonC(self, button):
        print(self)
        print("Button C")
        note_on = [0x90, 3, 1]
        note_off = [0x80, 3, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonD(self, button):
        print(self)
        print("Button D")
        note_on = [0x90, 4, 1]
        note_off = [0x80, 4, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonE(self, button):
        print(self)
        print("Button E")
        note_on = [0x90, 5, 1]
        note_off = [0x80, 5, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonF(self, button):
        print(self)
        print("Button F")
        note_on = [0x90, 6, 1]
        note_off = [0x80, 6, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonG(self, button):
        print(self)
        print("Button G")
        note_on = [0x90, 7, 1]
        note_off = [0x80, 7, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def flatButtonH(self, button):
        print(self)
        print("Button H")
        note_on = [0x90, 8, 1]
        note_off = [0x80, 8, 0]
        self.midiout.send_message(note_on)
        time.sleep(0.5)
        self.midiout.send_message(note_off)

    def dialAAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 16, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialBAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 17, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialCAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 18, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialDAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 19, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialEAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 20, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialFAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 21, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialGAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 22, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

    def dialHAction(self, value):
        print(self)
        print(value)
        note_on = [0x90, 23, round(value / 99 * 128)]
        self.midiout.send_message(note_on)

def setStylesheet(app):
    app.setStyleSheet("QPushButton { width: 8ex; }")

if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    setStylesheet(app)

    midiWidgets = MidiWidgets()
    midiWidgets.show()

    sys.exit(app.exec_())
